# python

## Sobre
Este repositório contém os códigos do curso de Python do Curso em Vídeo. O curso é ministrado por Gustavo Guanabara, um professor de programação muito popular no Brasil. O curso é gratuito e está disponível no YouTube.

## Códigos
Os códigos do curso estão divididos em três mundos:

Mundo 1: Fundamentos
Mundo 2: Estruturas de Controle
Mundo 3: Estruturas Compostas
Cada mundo contém uma série de aulas, cada uma com seu próprio código. Os códigos estão bem organizados e comentados, o que facilita seu aprendizado.

## Como Usar
Para usar os códigos, você precisa ter instalado o Python em sua máquina. Você pode baixar o Python do site oficial.

Depois de instalar o Python, você pode abrir os códigos em um editor de texto. Você também pode usar um IDE (Integrated Development Environment) como o PyCharm ou o Visual Studio Code.

Para executar os códigos, você pode usar o comando python nome_do_arquivo.py no terminal.

## Contribuições
Se você tiver alguma contribuição para o repositório, fique à vontade para enviar uma pull request.

## Licença
Os códigos deste repositório estão licenciados sob a licença MIT.
